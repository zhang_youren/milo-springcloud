package com.milo.springcloud.service;

import com.milo.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Milo
 * @Description
 * @Date 2020-05-10 22:58
 **/
public interface PaymentService {
    public int create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
