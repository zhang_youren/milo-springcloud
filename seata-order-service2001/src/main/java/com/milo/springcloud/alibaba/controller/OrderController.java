package com.milo.springcloud.alibaba.controller;

import com.milo.springcloud.alibaba.domain.CommonResult;
import com.milo.springcloud.alibaba.domain.Order;
import com.milo.springcloud.alibaba.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author Milo
 * @Description
 * @Date 2020-05-15 16:38
 **/
@RestController
public class OrderController
{
    @Resource
    private OrderService orderService;


    @GetMapping("/order/create")
    public CommonResult create(Order order)
    {
        orderService.create(order);
        return new CommonResult(200,"订单创建成功");
    }
}
